# Activity 0 : GitLab Setup for Assignments

Instructor : Homeyra Pourmohammadali
MTE 140 - Data Structures and Algorithms
Winter 2021 (Online)

University of Waterloo

Due: 11:00pm, Friday, January 29 2021

## Purpose of this activity

In this activity, you will practice setting up an online repository and version control system (GitLab at UWaterloo). It will be used for all future assignments. Version control systems (VCS) are widely used to manage programming projects. This activity is not graded and is only needed for working on your assignments.

## Instruction

### Step 1
Sign in to GitLab at UW ```https://git.uwaterloo.ca/users/sign_in``` with your WatIAM credentials (without @uwaterloo.ca).

### Step 2
Go to the public repository for MTE 140, at ```https://git.uwaterloo.ca/d24lau/mte140-w21```.

### Step 3
Fork the project to your own account.

Click on the 'Fork' button at the top right of the screen.

Click the 'Select' button underneath your name.

You now have a copy of the MTE 140 repo in your git.uwaterloo.ca (GitLab) account.

### Step 4
Clone the project to your own computer.

From within CLion, you are going to use the integrated VCS. From the CLion menu bar, select "VCS - Get from Version Control..." - "Use Version control: Git" and paste the URL of your repository. For example ```https://git.uwaterloo.ca/<userid>/mte140-w21```. Then click 'Clone'. You will need to provide your WatIAM login info.

Now you should have a local copy of the project files.
